package com.team12.socialnetwork.services;

import com.team12.socialnetwork.entities.DTOmodels.PostEditDTO;
import com.team12.socialnetwork.entities.DTOmodels.PostDTO;
import com.team12.socialnetwork.entities.Friend;
import com.team12.socialnetwork.entities.Post;
import com.team12.socialnetwork.entities.UserInfo;
import com.team12.socialnetwork.entities.enums.PrivacyType;
import com.team12.socialnetwork.mapper.PostMapper;
import com.team12.socialnetwork.repositories.base.PostsRepository;
import com.team12.socialnetwork.services.base.FriendsService;
import com.team12.socialnetwork.services.base.UsersService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import javax.persistence.EntityNotFoundException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.team12.socialnetwork.Factory.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.times;


@RunWith(MockitoJUnitRunner.class)
public class PostsServiceImplTests {
    @Mock
    private PostsRepository repository;

    @Mock
    private UsersService usersService;

    @Mock
    private Principal principal;

    @Mock
    private FriendsService friendsService;

    @Mock
    private PostMapper postMapper;

    @InjectMocks
    private PostsServiceImpl mockService;

    @Test
    public void createShould_CallRepository_WhenUserExists() {
        // Arrange
        UserInfo userInfo = createUser();
        Post post = createPost();
        PostDTO postDTO = createPostDTO();

        // Act
        Mockito.when(postMapper.PostDTOToPost(postDTO)).thenReturn(post);
        Mockito.when(principal.getName()).thenReturn("test@test.com");
        Mockito.when(usersService.getByEmail(anyString())).thenReturn(userInfo);
        mockService.create(postDTO,principal);

        // Assert
        Mockito.verify(repository,
                times(1)).save(post);
    }

    @Test
    public void updateShould_CallRepository_WhenUserExists() {
        // Arrange
        Post post = createPost();
        PostDTO postDTO = createPostDTO();

        // Act
        Mockito.when(repository.getPostById(anyInt())).thenReturn(post);
        Mockito.when(usersService.checkIfUserExists(anyInt())).thenReturn(true);
        mockService.update(post.getId(), postDTO);

        // Assert
        Mockito.verify(repository,
                times(1)).save(post);
    }

    @Test
    public void updateShould_ThrowException_WhenUserDoNotExists() {
        // Arrange
        Post post = createPost();
        PostDTO postDTO = createPostDTO();

        // Act
        Mockito.when(repository.getPostById(anyInt())).thenReturn(post);
        Mockito.when(usersService.checkIfUserExists(anyInt())).thenReturn(false);

        // Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> mockService.update(post.getId(), postDTO));
    }

    @Test
    public void deleteShould_CallRepository_WhenUserExists() {
        // Arrange
        Post post = createPost();

        // Act
        Mockito.when(repository.existsById(anyInt())).thenReturn(true);
        mockService.delete(post);

        // Assert
        Mockito.verify(repository,
                times(1)).delete(post);
    }

    @Test
    public void deleteShould_ThrowException_WhenUserDoNotExists() {
        // Arrange
        Post post = createPost();

        // Act
        Mockito.when(repository.existsById(anyInt())).thenReturn(false);

        // Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> mockService.delete(post));
    }

    @Test
    public void getAllPublicShould_CallRepository() {
        //Arrange
        Post post = createPost();

        //Act
        Mockito.when(repository.getAllByPrivacyOrderByTimeCreatedDesc(PrivacyType.PUBLIC)).thenReturn(Collections.singletonList(post));
        List<Post> returnedPosts = mockService.getPublicPosts();

        //Act, Assert
        Assert.assertSame(1, returnedPosts.size());
    }

    @Test
    public void getPostByIdShould_ReturnPost_WhenPostExists() {
        // Arrange
        Post expected = createPost();

        // Act
        Mockito.when(repository.existsById(anyInt())).thenReturn(true);
        Mockito.when(repository.getPostById(1)).thenReturn(expected);
        Post returned = mockService.getPostById(1);

        // Assert
        Assert.assertSame(expected, returned);
    }

    @Test
    public void getPostByIdShould_ThrowException_WhenPostDoNotExists() {
        // Arrange, Act
        Mockito.when(repository.existsById(anyInt())).thenReturn(false);

        // Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> mockService.getPostById(anyInt()));
    }

    @Test
    public void getAllPublicPostsShould_CallRepository() {
        //Arrange
        Post post = createPost();

        //Act
        Mockito.when(repository.getAllByPrivacyOrderByTimeCreatedDesc(post.getPrivacy()))
                .thenReturn(Collections.singletonList(post));
        List<Post> returnedPosts = mockService.getPublicPosts();

        //Act, Assert
        Assert.assertSame(1, returnedPosts.size());
    }

    @Test
    public void getPostByCreatorShould_ReturnCreatorPost_WhenPostExists() {
        // Arrange
        Post expected = createPost();
        UserInfo user =createUser();
        UserInfo userTwo = createUserTwo();
        Friend friend = createFriend();

        // Act
        Mockito.when(principal.getName()).thenReturn("test@test.com");
        Mockito.when(usersService.checkIfUserExists(anyInt())).thenReturn(true);
        Mockito.when(usersService.getByEmail(anyString())).thenReturn(userTwo);
        Mockito.when(usersService.getUserById(anyInt())).thenReturn(user);
        Mockito.when(repository.getAllByCreatedByOrderByTimeCreatedDesc(user))
                .thenReturn(Collections.singletonList(expected));
        Mockito.when(friendsService.getBySenderIdAndReceiverId(anyInt(),anyInt())).thenReturn(friend);
        List<Post> returnedPosts = mockService.getCreatorPosts(expected.getCreatedBy().getId(),principal);

        // Assert
        Assert.assertSame(1, returnedPosts.size());
    }

    @Test
    public void getPostByCreatorShould_ReturnPublicCreatorPost_WhenPostExists() {
        // Arrange
        Post expected = createPost();
        UserInfo user =createUser();
        UserInfo userTwo = createUserTwo();

        // Act
        Mockito.when(principal.getName()).thenReturn("test@test.com");
        Mockito.when(usersService.checkIfUserExists(anyInt())).thenReturn(true);
        Mockito.when(usersService.getByEmail(anyString())).thenReturn(userTwo);
        Mockito.when(usersService.getUserById(anyInt())).thenReturn(user);
        Mockito.when(repository.getAllByPrivacyAndCreatedByOrderByTimeCreatedDesc(PrivacyType.PUBLIC,user))
                .thenReturn(Collections.singletonList(expected));
        Mockito.when(friendsService.getBySenderIdAndReceiverId(anyInt(),anyInt())).thenReturn(null);
        List<Post> returnedPosts = mockService.getCreatorPosts(expected.getCreatedBy().getId(),principal);

        // Assert
        Assert.assertSame(1, returnedPosts.size());
    }

    @Test
    public void getPostByCreatorShould_ThrowException_WhenUserDoNotExists() {
        // Arrange, Act
        Mockito.when(usersService.checkIfUserExists(anyInt())).thenReturn(false);

        // Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> mockService.getCreatorPosts(anyInt(),principal));
    }

    @Test
    public void editPostShould_EditPostWhen_postExists() {
        //Arrange
        PostEditDTO postEditDTO = createPostEditModel();
        Post post = createPost();

        //Act
        Mockito.when(repository.getPostById(anyInt())).thenReturn(post);
        mockService.editPost(postEditDTO);

        //Act, Assert
        Assert.assertSame(postEditDTO.getContent(),post.getContent() );
    }

    @Test
    public void getAllShould_CallRepository() {
        //Arrange
        Post post = createPost();

        //Act
        Mockito.when(repository.findAll()).thenReturn(Collections.singletonList(post));
        List<Post> returnedPosts = mockService.getAll();

        //Act, Assert
        Assert.assertSame(1, returnedPosts.size());
    }

    @Test
    public void getAllFriendsPosts_ShouldReturnPosts_WhenPostsExists() {
        //Arrange
        Post post = createPost();
        UserInfo userInfo = createUser();
        Friend friend = createFriend();
        List<Post> posts = new ArrayList<>();
        posts.add(post);
        List<Friend> temp = new ArrayList<>();
        temp.add(friend);

        //Act
        Mockito.when(principal.getName()).thenReturn("test@test.com");
        Mockito.when(usersService.getByEmail(anyString())).thenReturn(userInfo);
        Mockito.when(friendsService.getAllBySenderId(anyInt())).thenReturn(temp);
        Mockito.when(repository.getAllByPrivacyOrderByTimeCreatedDesc(PrivacyType.PUBLIC)).thenReturn(posts);
       List<Post> expected= mockService.getAllFriendsPosts(principal);


        //Act, Assert
        Assert.assertSame(expected.size(),temp.size() );
    }

    @Test
    public void getAllPublicPostsByInteractio_CallRepository() {
        //Arrange
        Post post = createPost();

        //Act
        Mockito.when(repository.getAllByPrivacyOrderByTotalInteractionsDesc(PrivacyType.PUBLIC)).thenReturn(Collections.singletonList(post));
        List<Post> returnedPosts = mockService.getAllByPrivacyOrderByTotalInteractionsDesc(PrivacyType.PUBLIC);

        //Act, Assert
        Assert.assertSame(1, returnedPosts.size());
    }

    @Test
    public void getByCreatorShould_CallRepository() {
        //Arrange
        Post post = createPost();
        UserInfo userInfo = createUser();

        //Act
        Mockito.when(usersService.checkIfUserExists(anyInt())).thenReturn(true);
        Mockito.when(usersService.getUserById(anyInt())).thenReturn(userInfo);
        Mockito.when(repository.getAllByCreatedByOrderByTimeCreatedDesc(userInfo)).thenReturn(Collections.singletonList(post));
        List<Post> returnedPosts = mockService.getAllByCreatorSorted(anyInt());

        //Act, Assert
        Assert.assertSame(1, returnedPosts.size());
    }
    @Test
    public void getByCreatorShould_ThrowExceptionWhen_UserDoNotExist() {
        //Arrange
        Post post = createPost();
        UserInfo userInfo = createUser();

        //Act
        Mockito.when(usersService.checkIfUserExists(anyInt())).thenReturn(false);

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> mockService.getAllByCreatorSorted(anyInt()));
    }
}


