package com.team12.socialnetwork.services;

import com.team12.socialnetwork.entities.Friend;
import com.team12.socialnetwork.entities.UserInfo;
import com.team12.socialnetwork.exceptions.DuplicateEntityException;
import com.team12.socialnetwork.repositories.base.FriendsRepository;
import com.team12.socialnetwork.services.base.UsersService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.security.Principal;

import static com.team12.socialnetwork.Factory.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class FriendsServiceImplTests {
    @Mock
    private FriendsRepository repository;

    @Mock
    private UsersService usersService;

    @Mock
    private Principal principal;

    @InjectMocks
    private FriendsServiceImpl mockService;

    @Test
    public void createShould_CallRepository_WhenFriendDoNotExists() {
        // Arrange
        Friend expected = createFriend();

        // Act
        mockService.createFriendship(expected);

        // Assert
        Mockito.verify(repository,
                times(1)).save(expected);
    }

    @Test
    public void createShould_ThrowException_WhenFriendAlreadyExists() {
        // Arrange
        Friend expected = createFriend();
        Mockito.when(repository.existsById(anyInt()))
                .thenReturn(true);

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> mockService.createFriendship(expected));
    }

    @Test
    public void deleteShould_CallRepository_WhenFriendExists() {
        // Arrange
        Friend expected = createFriend();

        // Act
        mockService.deleteFriendship(expected);

        // Assert
        Mockito.verify(repository,
                times(1)).delete(expected);
    }

    @Test
    public void getBySenderIdAndReceiverIdShould_ReturnFriend_WhenFriendExists() {
        // Arrange
        Friend expected = createFriend();
        int senderId = expected.getSender().getId();
        int receiverId = expected.getReceiver().getId();

        Mockito.when(repository.getBySenderIdAndReceiverId(senderId, receiverId))
                .thenReturn(expected);

        // Act
        Friend returned = mockService.getBySenderIdAndReceiverId(senderId, receiverId);

        // Assert
        Assert.assertSame(expected, returned);
    }

    @Test
    public void getAllBySenderIdShould_CallRepository() {
        //Arrange
        mockService.getAllBySenderId(1);

        //Act, Assert
        Mockito.verify(repository, times(1))
                .getAllBySenderId(1);
    }

    @Test
    public void getAllByReceiverIdShould_CallRepository() {
        //Arrange
        mockService.getAllByReceiverId(1);

        //Act, Assert
        Mockito.verify(repository, times(1))
                .getAllByReceiverId(1);
    }

    @Test
    public void deleteFriendRequestShould_DeleteRequest() {
        //Arrange
        UserInfo sender = createUser();
        UserInfo receiver = createUserTwo();
        Friend friend = createFriend();

        //Act
        Mockito.when(usersService.getByEmail(principal.getName())).thenReturn(sender);
        Mockito.when(usersService.getUserById(receiver.getId())).thenReturn(receiver);
        Mockito.when(repository.getBySenderIdAndReceiverId(sender.getId(),receiver.getId())).thenReturn(friend);

        mockService.deleteFriend(principal, receiver.getId());

        //Assert
        Mockito.verify(repository, Mockito.times(2)).delete(Mockito.any());
    }

}
