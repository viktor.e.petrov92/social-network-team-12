package com.team12.socialnetwork.services;

import com.team12.socialnetwork.entities.DTOmodels.LikeCommentDTO;
import com.team12.socialnetwork.entities.Comment;
import com.team12.socialnetwork.entities.LikeComment;
import com.team12.socialnetwork.entities.UserInfo;
import com.team12.socialnetwork.exceptions.DuplicateEntityException;
import com.team12.socialnetwork.repositories.base.CommentsRepository;
import com.team12.socialnetwork.repositories.base.LikeCommentRepository;
import com.team12.socialnetwork.services.base.CommentsService;
import com.team12.socialnetwork.services.base.UsersService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import javax.persistence.EntityNotFoundException;
import java.security.Principal;

import static com.team12.socialnetwork.Factory.*;

@RunWith(MockitoJUnitRunner.class)
public class LikeCommentServiceImplTests {

    @Mock
    private LikeCommentRepository repository;

    @Mock
    private Principal principal;

    @Mock
    private UsersService usersService;

    @Mock
    private CommentsRepository commentsRepository;

    @Mock
    private CommentsService commentsService;


    @InjectMocks
    private LikeCommentServiceImpl mockService;

    @Test
    public void createShould_CallRepository() {
        // Arrange
        LikeComment expected = createLikeComment();

        // Act
        mockService.createLikeComment(expected);

        // Assert

        Mockito.verify(repository, Mockito.times(1)).save(expected);
    }

    @Test
    public void deleteShould_CallRepository() {
        // Arrange
        LikeComment expected = createLikeComment();

        // Act
        mockService.deleteLikeComment(expected);

        // Assert

        Mockito.verify(repository, Mockito.times(1)).delete(expected);
    }


    @Test
    public void likeCommentShould_IncreaseLikeCount() {
        // Arrange
        Comment comment = createComment();
        UserInfo user = createUser();
        LikeCommentDTO likeCommentDTO = createLikeCommentDTO();

        // Act

        Mockito.when(commentsService.getCommentById(likeCommentDTO.getCommentId())).thenReturn(comment);
        Mockito.when(usersService.getByEmail(principal.getName())).thenReturn(user);
        mockService.likeComment(likeCommentDTO, principal);

        //Assert

        Assert.assertEquals(1, comment.getTotalLikes());

    }


    @Test
    public void existsByUserAndComment_ThrowException_WhenLikeCommentAlreadyExists() {
        // Arrange
        LikeComment expected = createLikeComment();
        UserInfo user = expected.getUser();
        Comment comment = expected.getComment();
        LikeCommentDTO likeCommentDTO = createLikeCommentDTO();


        // Act

        Mockito.when(commentsService.getCommentById(likeCommentDTO.getCommentId())).thenReturn(comment);
        Mockito.when(usersService.getByEmail(principal.getName())).thenReturn(user);
        Mockito.when(repository.existsByUserAndComment(user, comment)).thenReturn(true);

        // Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> mockService.likeComment(likeCommentDTO, principal));
    }

    @Test
    public void dislikeCommentShould_DecreaseLikeCount() {
        // Arrange
        Comment comment = createComment();
        UserInfo user = createUser();
        LikeCommentDTO likeCommentDTO = createLikeCommentDTO();
        LikeComment likeComment = createLikeComment();


        // Act
        Mockito.when(commentsService.getCommentById(likeCommentDTO.getCommentId())).thenReturn(comment);
        Mockito.when(usersService.getByEmail(principal.getName())).thenReturn(user);
        Mockito.when(repository.getByUserAndComment(user, comment)).thenReturn(likeComment);
        Mockito.when(repository.existsByUserAndComment(user, comment)).thenReturn(true);
        mockService.dislikeComment(likeCommentDTO, principal);

        //Assert
        Assert.assertEquals(-1, comment.getTotalLikes());

    }


    @Test
    public void existsByUserAndComment_ThrowException_WhenLikeCommentNotExist() {
        // Arrange
        LikeComment expected = createLikeComment();
        UserInfo user = expected.getUser();
        Comment comment = expected.getComment();
        LikeCommentDTO likeCommentDTO = createLikeCommentDTO();


        // Act

        Mockito.when(commentsService.getCommentById(likeCommentDTO.getCommentId())).thenReturn(comment);
        Mockito.when(usersService.getByEmail(principal.getName())).thenReturn(user);
        Mockito.when(repository.existsByUserAndComment(user, comment)).thenReturn(false);

        // Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> mockService.dislikeComment(likeCommentDTO, principal));
    }


}
