package com.team12.socialnetwork.services;

import com.team12.socialnetwork.entities.DTOmodels.RegistrationDTO;
import com.team12.socialnetwork.entities.Login;
import com.team12.socialnetwork.repositories.base.LoginRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;

import static com.team12.socialnetwork.Factory.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;


@RunWith(MockitoJUnitRunner.class)
public class LoginServiceImplTests {
    @Mock
    private LoginRepository repository;

    @Mock
    private UserDetailsManager detailsManager;

    @Mock
    private PasswordEncoder passwordEncoder;

    @InjectMocks
    private LoginServiceImpl mockService;

    @Test
    public void findByUsernameShould_ReturnUser_WhenUserExists() {
        //Arrange
        Login login = createLogin();

        //Act
        Mockito.when(repository.findById(anyString())).thenReturn(java.util.Optional.of(login));

        //Assert
        Assert.assertEquals(login, mockService.findByUsername(anyString()));
    }

    @Test
    public void existByUserNameShould_returnTrueWhen_UserExists(){
        //Arrange, Act
        Mockito.when(repository.existsByUsername(anyString())).thenReturn(true);

        //Assert
        Assert.assertTrue(mockService.existsByUsername(anyString()));
    }

    @Test
    public void createLoginShould_CreateNewLogin() {
        //Arrange, Act
        RegistrationDTO registrationDTO = createRegModel();
        User newUser = createSecUser();
        Mockito.when(passwordEncoder.encode(anyString())).thenReturn(registrationDTO.getPassword());
        mockService.createLogin(registrationDTO);

        //Assert
        Mockito.verify(detailsManager,
                times(1)).createUser(newUser);
    }

    @Test
    public void updatePasswordShould_CallRepository(){
        //Arrange, Act
        Login login = createLogin();
        mockService.updatePassword(login);

        //Assert
        Mockito.verify(repository,times(1)).save(login);
    }
}
