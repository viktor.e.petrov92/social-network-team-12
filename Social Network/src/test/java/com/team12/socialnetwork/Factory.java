package com.team12.socialnetwork;

import com.team12.socialnetwork.entities.DTOmodels.*;
import com.team12.socialnetwork.entities.*;
import com.team12.socialnetwork.entities.enums.PrivacyType;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.List;

public class Factory {
    private static PasswordEncoder passwordEncoder;

    public Factory(PasswordEncoder passwordEncoder) {
        Factory.passwordEncoder = passwordEncoder;
    }

    public static UserInfo createUser() {
        UserInfo userInfo = new UserInfo();
        userInfo.setId(1);
        userInfo.setFirstName("firstNameTest");
        userInfo.setLastName("lastNameTest");
        userInfo.setLogin(createLogin());
        userInfo.setAge(22);
        userInfo.setAddress("addressTest");
        userInfo.setEducation("educationTest");
        userInfo.setPrivacy(PrivacyType.PUBLIC);
        return userInfo;
    }

    public static UserInfo createUserTwo() {
        UserInfo userInfo = new UserInfo();
        userInfo.setId(2);
        userInfo.setFirstName("firstNameTest2");
        userInfo.setLastName("lastNameTest2");
        userInfo.setLogin(createLogin());
        userInfo.setAge(23);
        userInfo.setAddress("addressTest2");
        userInfo.setEducation("educationTest2");
        userInfo.setPrivacy(PrivacyType.PUBLIC);
        return userInfo;
    }

    public static Post createPost() {
        Post post = new Post();
        post.setId(1);
        post.setContent("contentTest");
        post.setCreatedBy(createUser());
        post.setPrivacy(PrivacyType.PUBLIC);
        post.setTotalInteractions(1);
        post.setComments(new ArrayList<>());
        return post;
    }

    public static Comment createComment() {
        Comment comment = new Comment();
        comment.setId(1);
        comment.setContent("content");
        comment.setCreator(createUser());
        comment.setPost(createPost());
        return comment;
    }

    public static CommentDTO createCommentDTO() {
        CommentDTO commentDTO = new CommentDTO();
        commentDTO.setContent("content");
        commentDTO.setPostId(1);
        return commentDTO;
    }

    public static LikeComment createLikeComment() {
        LikeComment likeComment = new LikeComment();
        likeComment.setId(1);
        likeComment.setUser(createUser());
        likeComment.setComment(createComment());
        return likeComment;
    }

    public static LikeCommentDTO createLikeCommentDTO() {
        LikeCommentDTO likeCommentDTO = new LikeCommentDTO();
        likeCommentDTO.setCommentId(1);

        return likeCommentDTO;
    }

    public static LikePost createLikePost() {
        LikePost likePost = new LikePost();
        likePost.setId(1);
        likePost.setPost(createPost());
        likePost.setUser(createUser());
        return likePost;
    }

    public static LikePostDTO createLikePostDTO() {
        LikePostDTO likePostDTO = new LikePostDTO();
        likePostDTO.setPostId(1);

        return likePostDTO;
    }

    public static Request createRequest() {
        Request request = new Request();
        request.setRequestId(1);
        request.setSender(createUser());
        request.setReceiver(createUserTwo());
        return request;
    }

    public static Friend createFriend() {
        Friend friend = new Friend();
        friend.setFriendId(1);
        friend.setSender(createUser());
        friend.setReceiver(createUserTwo());
        return friend;
    }

    public static UserDTO createUserDTO() {
        UserDTO userDTO = new UserDTO();
        userDTO.setFirstName("test");
        userDTO.setLastName("test");
        userDTO.setEmail("test");
        userDTO.setPassword("123123123");
        userDTO.setPrivacy(PrivacyType.PUBLIC);
        return userDTO;
    }

    public static PostDTO createPostDTO() {
        PostDTO postDTO = new PostDTO();
        postDTO.setContent("test");
        postDTO.setPictureValue("testString");
        postDTO.setPrivacy(PrivacyType.PUBLIC);
        return postDTO;
    }

    public static Login createLogin() {
        Login login = new Login();
        login.setUsername("test@test.com");
        login.setPassword("password");
        login.setEnabled(true);
        return login;
    }

    public static RegistrationDTO createRegModel() {
        RegistrationDTO registrationDTO = new RegistrationDTO();
        registrationDTO.setEmail("test@test.com");
        registrationDTO.setFirstName("test");
        registrationDTO.setLastName("test");
        registrationDTO.setPassword("password");
        registrationDTO.setAddress("address");
        registrationDTO.setEducation("education");
        registrationDTO.setAge(22);
        return registrationDTO;
    }

    public static AdditionalInfoDTO createAddInfoDTO() {
        AdditionalInfoDTO additionalInfoDTO = new AdditionalInfoDTO();
        additionalInfoDTO.setFirstName("test");
        additionalInfoDTO.setLastName("test");
        return additionalInfoDTO;
    }

    public static User createSecUser() {
        RegistrationDTO registrationDTO = createRegModel();
        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        return new User(registrationDTO.getEmail(), registrationDTO.getPassword(), false,
                true, true, true, authorities);
    }

    public static CommentEditDTO createEditCommentModel(){
        CommentEditDTO model = new CommentEditDTO();
        model.setContent("content");
        model.setId(1);
        model.setTotalLikes(0);
        return model;
    }

    public static PostEditDTO createPostEditModel(){
        PostEditDTO postEditDTO = new PostEditDTO();
        postEditDTO.setId(1);
        postEditDTO.setContent("modelContent");
        return postEditDTO;
    }

    public static PasswordChangeDTO createPWChangeModel(){
        PasswordChangeDTO passwordChangeDTO = new PasswordChangeDTO();
        passwordChangeDTO.setNewPassword("newPassword");
        return passwordChangeDTO;
    }
}
