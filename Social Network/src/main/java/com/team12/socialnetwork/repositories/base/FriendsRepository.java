package com.team12.socialnetwork.repositories.base;

import com.team12.socialnetwork.entities.Friend;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FriendsRepository extends JpaRepository<Friend, Integer> {

    Friend getBySenderIdAndReceiverId(int userId, int friendId);

    List<Friend> getAllBySenderId(int senderId);

    List<Friend> getAllByReceiverId(int receiverId);
}
