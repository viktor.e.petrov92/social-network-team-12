package com.team12.socialnetwork.repositories.base;

import com.team12.socialnetwork.entities.Request;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RequestsRepository extends JpaRepository<Request, Integer> {
    Request getBySenderIdAndReceiverId(int senderId, int receiverId);

    List<Request> getAllBySenderId(int senderId);

    List<Request> getAllByReceiverId(int receiverId);
}
