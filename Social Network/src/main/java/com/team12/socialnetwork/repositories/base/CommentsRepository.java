package com.team12.socialnetwork.repositories.base;

import com.team12.socialnetwork.entities.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentsRepository extends JpaRepository<Comment, Integer> {

    Comment getCommentById(int commentId);

    List<Comment> getAllByPostId(int postId);
}
