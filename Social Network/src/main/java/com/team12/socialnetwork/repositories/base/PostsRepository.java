package com.team12.socialnetwork.repositories.base;

import com.team12.socialnetwork.entities.Post;
import com.team12.socialnetwork.entities.UserInfo;
import com.team12.socialnetwork.entities.enums.PrivacyType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostsRepository extends JpaRepository<Post, Integer> {

    Post getPostById(int postId);

    List<Post> getAllByPrivacyAndCreatedByOrderByTimeCreatedDesc(PrivacyType privacyType, UserInfo creator);

    List<Post> getAllByPrivacyOrderByTotalInteractionsDesc(PrivacyType privacyType);

    List<Post> getAllByPrivacyOrderByTimeCreatedDesc(PrivacyType privacy);

    List<Post> getAllByCreatedByOrderByTimeCreatedDesc(UserInfo creator);
}
