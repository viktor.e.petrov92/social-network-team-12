package com.team12.socialnetwork.repositories.base;

import com.team12.socialnetwork.entities.LikePost;
import com.team12.socialnetwork.entities.Post;
import com.team12.socialnetwork.entities.UserInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LikePostRepository extends JpaRepository<LikePost, Integer> {

    LikePost getByUserAndPost(UserInfo user, Post post);

    boolean existsByUserAndPost(UserInfo user, Post post);
}
