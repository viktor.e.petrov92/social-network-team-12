package com.team12.socialnetwork.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.team12.socialnetwork.entities.enums.PrivacyType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.tomcat.util.codec.binary.Base64;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "users_info")
public class UserInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @JsonIgnore
    @OneToOne(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "email")
    @NotNull
    private Login login;

    @Column(name = "age")
    private Integer age;

    @Column(name = "address")
    private String address;

    @Column(name = "education")
    private String education;

    @Enumerated(EnumType.STRING)
    private PrivacyType privacy;

    @Column(name = "profile_photo")
    private byte[] profilePicture;

    @JsonIgnore
    @OneToMany
    @JoinColumn(name = "sender_id")
    private List<Request> senderRequests;

    @JsonIgnore
    @OneToMany
    @JoinColumn(name = "receiver_id")
    private List<Request> receivedRequests;

    @JsonIgnore
    @OneToMany
    @JoinColumn(name = "sender_id")
    private List<Friend> senderFriends;

    @JsonIgnore
    @OneToMany
    @JoinColumn(name = "receiver_id")
    private List<Friend> receivedFriends;

    @JsonIgnore
    @OneToMany
    @JoinColumn(name = "user_id")
    private List<LikePost> likePosts;

    @JsonIgnore
    @OneToMany
    @JoinColumn(name = "user_id")
    private List<LikeComment> likeComments;

    @JsonIgnore
    @Column(name = "cover_photo")
    private byte[] coverPhoto;

    public String generateCoverPhoto() {
        return Base64.encodeBase64String(this.getCoverPhoto());
    }

    public String generateProfilePhoto() {
        return Base64.encodeBase64String(this.getProfilePicture());
    }
}

