package com.team12.socialnetwork.entities.DTOmodels;

import com.team12.socialnetwork.entities.enums.PrivacyType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class AdditionalInfoDTO {

    String firstName;
    String lastName;
    Integer age;
    String address;
    String education;
    PrivacyType privacy;
}
