package com.team12.socialnetwork.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.team12.socialnetwork.entities.enums.PrivacyType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "post_info")
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "text_post")
    private String content;

    @Column(name = "picture_post")
    private byte[] picturePost;

    @Enumerated(EnumType.STRING)
    private PrivacyType privacy;

    @Column(name = "time_created")
    private String timeCreated;

    @ManyToOne
    @JoinColumn(name = "created_by_id")
    private UserInfo createdBy;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "for_post_id")
    private List<Comment> comments;

    @JsonIgnore
    @OneToMany
    @JoinColumn(name = "post_id")
    private List<LikePost> likePosts;

    @Column(name = "total_likes")
    private int totalLikes;

    @Column(name = "total_interactions")
    private int totalInteractions;
}
