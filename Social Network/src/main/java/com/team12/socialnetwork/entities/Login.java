package com.team12.socialnetwork.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "users")
public class Login {

    @Id
    @Column(name = "username")
    @Size(min = 3, max = 50)
    private String username;

    @Column(name = "password")
    @Size(min = 3, max = 255)
    private String password;

    @Column(name = "enabled")
    private boolean enabled;

    public Login(@Size(min = 3, max = 50) String username, @Size(min = 6, max = 30) String password, boolean enabled) {
        this.username = username;
        this.password = password;
        this.enabled = enabled;
    }
}
