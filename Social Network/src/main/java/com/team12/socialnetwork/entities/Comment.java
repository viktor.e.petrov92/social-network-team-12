package com.team12.socialnetwork.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "comments")
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "comment")
    private String content;

    @OneToOne
    @JoinColumn(name = "created_by")
    private UserInfo creator;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "for_post_id")
    private Post post;

    @JsonIgnore
    @OneToMany
    @JoinColumn(name = "comment_id")
    private List<LikeComment> likeComments;

    @Column(name = "total_likes")
    private int totalLikes;

    @Column(name = "time_created")
    private String timeCreated;
}

