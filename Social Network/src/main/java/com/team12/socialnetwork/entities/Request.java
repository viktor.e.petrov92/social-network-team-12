package com.team12.socialnetwork.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "requests")
public class Request {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int requestId;

    @ManyToOne
    @JoinColumn(name = "sender_id")
    private UserInfo sender;

    @ManyToOne
    @JoinColumn(name = "receiver_id")
    private UserInfo receiver;

    public Request(UserInfo sender, UserInfo receiver) {
        this.sender = sender;
        this.receiver = receiver;
    }
}
