package com.team12.socialnetwork.entities.DTOmodels;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
public class CommentEditDTO {

    @PositiveOrZero
    private int id;

    @Size(max = 255, message = "Comment should be between 2 and 255 symbols")
    private String content;

    @PositiveOrZero
    private int totalLikes;
}
