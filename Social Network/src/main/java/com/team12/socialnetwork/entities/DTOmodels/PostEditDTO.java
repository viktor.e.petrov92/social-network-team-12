package com.team12.socialnetwork.entities.DTOmodels;

import com.team12.socialnetwork.entities.enums.PrivacyType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
public class PostEditDTO {

    @PositiveOrZero
    private int id;

    @Size(max = 255, message = "Comment should be between 2 and 255 symbols")
    private String content;

    private PrivacyType privacy;

    @PositiveOrZero
    private int totalLikes;
}
