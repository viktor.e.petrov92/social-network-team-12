package com.team12.socialnetwork.mapper;

import com.team12.socialnetwork.entities.DTOmodels.PostDTO;
import com.team12.socialnetwork.entities.Post;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

@Mapper
public interface PostMapper {

    Post PostDTOToPost(PostDTO postDTO);

    void PostDTOToPostUpdate(PostDTO postDTO, @MappingTarget Post post);
}
