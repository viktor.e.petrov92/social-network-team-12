package com.team12.socialnetwork.mapper;

import com.team12.socialnetwork.entities.DTOmodels.RegistrationDTO;
import com.team12.socialnetwork.entities.UserInfo;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

@Mapper
public interface UserMapper {

    UserInfo registrationModelToUserInfo(RegistrationDTO registrationDTO);

    void registrationModelToUserInfoUpdate(RegistrationDTO registrationDTO, @MappingTarget UserInfo userInfo);
}
