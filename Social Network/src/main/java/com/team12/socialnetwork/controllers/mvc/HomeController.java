package com.team12.socialnetwork.controllers.mvc;

import com.team12.socialnetwork.entities.DTOmodels.PostDTO;
import com.team12.socialnetwork.entities.UserInfo;
import com.team12.socialnetwork.entities.enums.PrivacyType;
import com.team12.socialnetwork.services.base.PostsService;
import com.team12.socialnetwork.services.base.UsersService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@RequiredArgsConstructor
public class HomeController {

    private final PostsService postsService;
    private final UsersService usersService;

    @GetMapping("/")
    public String showPublicHomePage(Model model) {
        model.addAttribute("posts", postsService.getPublicPosts());
        return "home";
    }

    @GetMapping("/timeline")
    public String showHomePageForLoggedIn(Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserInfo userInfo = usersService.getByEmail(authentication.getName());

        model.addAttribute("user", userInfo);
        model.addAttribute("posts", postsService.getPublicPosts());
        return "timeline";
    }

    @GetMapping("/hot")
    public String showMostPopularGlobally(Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserInfo userInfo = usersService.getByEmail(authentication.getName());

        model.addAttribute("postDTO", new PostDTO());
        model.addAttribute("user", userInfo);
        model.addAttribute("posts", postsService.getAllByPrivacyOrderByTotalInteractionsDesc(PrivacyType.PUBLIC));
        return "hot";
    }

    @GetMapping("/about")
    public String showAbout(Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserInfo userInfo = usersService.getByEmail(authentication.getName());
        model.addAttribute("user", userInfo);
        return "about";
    }
}