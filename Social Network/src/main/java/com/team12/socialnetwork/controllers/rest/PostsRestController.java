package com.team12.socialnetwork.controllers.rest;

import com.team12.socialnetwork.entities.DTOmodels.PostDTO;
import com.team12.socialnetwork.entities.Comment;
import com.team12.socialnetwork.entities.Post;
import com.team12.socialnetwork.entities.enums.PrivacyType;
import com.team12.socialnetwork.services.base.PostsService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/posts")
@RequiredArgsConstructor
public class PostsRestController {

    private final PostsService postsService;

    @GetMapping
    public List<Post> getAllPosts() {
        return postsService.getAll();
    }

    @GetMapping("/{id}")
    public Post getPostById(@PathVariable int id) {
        try {
            return postsService.getPostById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/creator/{id}")
    public List<Post> getByCreator(@PathVariable int id) {
        try {
            return postsService.getAllByCreatorSorted(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/profile/{id}")
    public List<Post> getByCreatorPublic(@PathVariable int id, Principal principal) {
        try {
            return postsService.getCreatorPosts(id, principal);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/public")
    public List<Post> getAllPublicPosts() {
        return postsService.getPublicPosts();
    }

    @GetMapping("/popular")
    public List<Post> getMostPopularPosts() {
        return postsService.getAllByPrivacyOrderByTotalInteractionsDesc(PrivacyType.PUBLIC);
    }

    @GetMapping("/time-line")
    public List<Post> getTimeLinePosts(Principal principal) {
        return postsService.getAllFriendsPosts(principal);
    }

    @PostMapping
    public Post create(@RequestBody @Valid PostDTO postDTO, Principal principal) {
        try {
            return postsService.create(postDTO, principal);
        } catch (EntityNotFoundException nfe) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, nfe.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Post update(@PathVariable int id,
                       @RequestBody @Valid PostDTO postDTO) {
        try {
            return postsService.update(id, postDTO);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        try {
            postsService.delete(postsService.getPostById(id));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
