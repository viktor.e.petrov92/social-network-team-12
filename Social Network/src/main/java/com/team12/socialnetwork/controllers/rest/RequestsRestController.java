package com.team12.socialnetwork.controllers.rest;

import com.team12.socialnetwork.entities.Request;
import com.team12.socialnetwork.exceptions.DuplicateEntityException;
import com.team12.socialnetwork.services.base.RequestsService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/requests")
@RequiredArgsConstructor
public class RequestsRestController {
    private final RequestsService requestsService;

    @GetMapping("/sender/{id}")
    public List<Request> getSenderRequests(@PathVariable Integer id) {
        return requestsService.getAllBySenderId(id);
    }

    @GetMapping("/receiver/{id}")
    public List<Request> getReceiverRequests(@PathVariable Integer id) {
        return requestsService.getAllByReceiverId(id);
    }

    @PostMapping
    public void sendFriendRequest(Principal principal,
                                  @RequestParam(value = "receiverId") Integer receiverId) {
        try {
            requestsService.sendFriendRequest(principal, receiverId);
        } catch (EntityNotFoundException nfe) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, nfe.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PostMapping("/accept")
    public void acceptFriendRequest(Principal principal,
                                    @RequestParam(value = "senderId") int senderId) {
        try {
            requestsService.acceptFriendRequest(principal, senderId);
        } catch (EntityNotFoundException nfe) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, nfe.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PostMapping("/decline")
    public void declineFriendRequest(Principal principal,
                                     @RequestParam(value = "senderId") int senderId) {
        try {
            requestsService.declineFriendRequest(principal, senderId);
        } catch (EntityNotFoundException nfe) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, nfe.getMessage());
        }
    }
}
