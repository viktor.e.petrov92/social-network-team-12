package com.team12.socialnetwork.controllers.mvc;

import com.team12.socialnetwork.entities.DTOmodels.AdditionalInfoDTO;
import com.team12.socialnetwork.entities.DTOmodels.CommentDTO;
import com.team12.socialnetwork.entities.DTOmodels.PostDTO;
import com.team12.socialnetwork.entities.UserInfo;
import com.team12.socialnetwork.entities.enums.PrivacyType;
import com.team12.socialnetwork.repositories.base.UsersRepository;
import com.team12.socialnetwork.services.base.FriendsService;
import com.team12.socialnetwork.services.base.PostsService;
import com.team12.socialnetwork.services.base.RequestsService;
import com.team12.socialnetwork.services.base.UsersService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;

@Controller
@RequiredArgsConstructor
public class UsersController {

    private final PostsService postsService;
    private final UsersService usersService;
    private final UsersRepository repository;
    private final FriendsService friendsService;
    private final RequestsService requestsService;

    @GetMapping("/profile")
    public String showUserInformation(Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.getPrincipal().equals("anonymousUser")) {
            return "landing";
        }
        UserInfo userInfo = usersService.getByEmail(authentication.getName());
        model.addAttribute("postDTO", new PostDTO());
        model.addAttribute("posts", postsService.getAllByCreatorSorted(userInfo.getId()));
        model.addAttribute("user", userInfo);
        return "profile";
    }

    @GetMapping("/profile/{id}/edit-password")
    public String showEditUserPassForm(@PathVariable int id, Model model) {
        UserInfo userInfo = usersService.getUserById(id);
        model.addAttribute("userInfo", userInfo);
        return "edit-password";
    }

    @GetMapping("/profile/{id}/edit-profile")
    public String showEditUserForm(@PathVariable int id, Model model) {

        UserInfo userInfo = usersService.getUserById(id);
        model.addAttribute("userInfo", userInfo);
        return "edit-profile";
    }


    @PostMapping("/profile/{id}/edit-profile")
    public String changeUserInfo(@Valid @ModelAttribute("model") AdditionalInfoDTO dto,
                                 BindingResult errors,
                                 Model model,
                                 @PathVariable("id") int id) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (errors.hasErrors()) {
            return "edit-profile";
        }
        UserInfo userInfo = usersService.getByEmail(authentication.getName());
        usersService.update(dto, userInfo.getLogin().getUsername());
        return "redirect:/profile";
    }

    @GetMapping("/profile/public/{id}")
    public String showOtherUsersProfilePublic(Model model, @PathVariable String id) {
        UserInfo userInfo = usersService.getUserById(Integer.parseInt(id));
        model.addAttribute("public", PrivacyType.PUBLIC);
        model.addAttribute("user", userInfo);
        return "profile-others-public";
    }

    @GetMapping("/profile/{id}")
    public String showOtherUsersProfile(Model model, @PathVariable String id) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserInfo loggedUser = usersService.getByEmail(authentication.getName());
        UserInfo userInfo = usersService.getUserById(Integer.parseInt(id));

        if (authentication.getPrincipal().equals("anonymousUser")) {
            return "redirect:/profile/public/{id}";
        }
        if (loggedUser.equals(userInfo)) {
            return "redirect:/profile";
        }
        model.addAttribute("loggerRequests",requestsService.getBySenderIdAndReceiverId(loggedUser.getId(),userInfo.getId()));
        model.addAttribute("loggerFriendExist", friendsService.getBySenderIdAndReceiverId(loggedUser.getId(), userInfo.getId()));
        model.addAttribute("userFriendExist", friendsService.getBySenderIdAndReceiverId(userInfo.getId(), loggedUser.getId()));
        model.addAttribute("public", PrivacyType.PUBLIC);
        model.addAttribute("commentDTO", new CommentDTO());
        model.addAttribute("loggedUser", loggedUser);
        model.addAttribute("user", userInfo);
        return "profile-others";
    }

    @PostMapping("/profile/photo")
    public String changePhoto(@Valid @RequestParam(name = "file") MultipartFile file) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserInfo userInfo = usersService.getByEmail(authentication.getName());
        if (!file.isEmpty()) {
            try {
                userInfo.setProfilePicture(file.getBytes());
                repository.save(userInfo);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return "redirect:/profile";
    }

    @PostMapping("/profile/coverPhoto")
    public String changeCoverPhoto(@Valid @RequestParam(name = "file") MultipartFile file) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserInfo userInfo = usersService.getByEmail(authentication.getName());
        if (!file.isEmpty()) {
            try {
                userInfo.setCoverPhoto(file.getBytes());
                repository.save(userInfo);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return "redirect:/profile";
    }
}
