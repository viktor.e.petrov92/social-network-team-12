package com.team12.socialnetwork.controllers.rest;

import com.team12.socialnetwork.entities.DTOmodels.CommentDTO;
import com.team12.socialnetwork.entities.Comment;
import com.team12.socialnetwork.services.base.CommentsService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/comments")
@RequiredArgsConstructor
public class CommentsRestController {

    private final CommentsService commentsService;

    @GetMapping
    public List<Comment> getAllComments() {
        return commentsService.getAllComments();
    }

    @GetMapping("/{id}")
    public Comment getCommentById(@PathVariable int id) {
        try {
            return commentsService.getCommentById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Comment create(Principal principal,
                          @RequestBody @Valid CommentDTO commentDTO) {
        return commentsService.create(commentDTO, principal);
    }

    @PutMapping("/{id}")
    public Comment update(@PathVariable int id,
                          @RequestBody @Valid CommentDTO commentDTO) {
        try {
            return commentsService.update(id, commentDTO);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        try {
            commentsService.delete(commentsService.getCommentById(id));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}