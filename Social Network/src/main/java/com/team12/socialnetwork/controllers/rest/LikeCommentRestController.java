package com.team12.socialnetwork.controllers.rest;

import com.team12.socialnetwork.entities.DTOmodels.LikeCommentDTO;
import com.team12.socialnetwork.exceptions.DuplicateEntityException;
import com.team12.socialnetwork.services.base.LikeCommentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import java.security.Principal;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/comment")
public class LikeCommentRestController {

    private final LikeCommentService likeCommentService;

    @PostMapping("/like")
    public int likeComment(@RequestBody @Valid LikeCommentDTO likeCommentDTO, Principal principal) {
        try {
            return likeCommentService.likeComment(likeCommentDTO, principal);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PostMapping("/dislike")
    public int dislikeComment(@RequestBody @Valid LikeCommentDTO likeCommentDTO, Principal principal) {
        try {
            return likeCommentService.dislikeComment(likeCommentDTO, principal);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
