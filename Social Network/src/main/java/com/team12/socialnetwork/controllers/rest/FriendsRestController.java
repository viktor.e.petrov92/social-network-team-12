package com.team12.socialnetwork.controllers.rest;

import com.team12.socialnetwork.services.base.FriendsService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;
import java.security.Principal;

@RestController
@RequestMapping("/api/friends")
@RequiredArgsConstructor
public class FriendsRestController {

    private final FriendsService friendsService;

    @GetMapping("/{id}")
    public void getUserFriends(@PathVariable Integer id) {
            friendsService.getAllBySenderId(id);
    }

    @PostMapping
    public void deleteFriend(Principal principal,
                             @RequestParam(value = "receiverId") int receiverId) {
        try {
            friendsService.deleteFriend(principal, receiverId);
        } catch (EntityNotFoundException nfe) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, nfe.getMessage());
        }
    }
}
