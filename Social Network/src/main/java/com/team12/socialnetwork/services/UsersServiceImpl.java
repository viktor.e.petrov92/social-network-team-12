package com.team12.socialnetwork.services;

import com.team12.socialnetwork.entities.DTOmodels.AdditionalInfoDTO;
import com.team12.socialnetwork.entities.DTOmodels.PasswordChangeDTO;
import com.team12.socialnetwork.entities.DTOmodels.RegistrationDTO;
import com.team12.socialnetwork.entities.Login;
import com.team12.socialnetwork.entities.UserInfo;
import com.team12.socialnetwork.entities.enums.PrivacyType;
import com.team12.socialnetwork.exceptions.DuplicateEntityException;
import com.team12.socialnetwork.mapper.AdditionalInfoMapper;
import com.team12.socialnetwork.mapper.UserMapper;
import com.team12.socialnetwork.repositories.base.UsersRepository;
import com.team12.socialnetwork.services.base.LoginService;
import com.team12.socialnetwork.services.base.UsersService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

import static java.lang.String.format;

@Service
@RequiredArgsConstructor
public class UsersServiceImpl implements UsersService {

    private final UsersRepository userRepository;
    private final UserMapper userMapper;
    private final AdditionalInfoMapper additionalInfoMapper;
    private final LoginService loginService;
    private final PasswordEncoder passwordEncoder;

    private static final String ALREADY_REGISTERED = "Someone already registered with that email!";
    private static final String USER_NOT_FOUND = "User with mail %s was not found!";

    @Override
    public UserInfo create(RegistrationDTO registrationDTO) {
        if (loginService.existsByUsername(registrationDTO.getEmail())) {
            throw new DuplicateEntityException(ALREADY_REGISTERED);
        }

        loginService.createLogin(registrationDTO);

        Login login = loginService.findByUsername(registrationDTO.getEmail());
        UserInfo newUserInfo = userMapper.registrationModelToUserInfo(registrationDTO);
        newUserInfo.setLogin(login);
        newUserInfo.setProfilePicture(toByteArrayProfilePhoto());
        newUserInfo.setCoverPhoto(toByteArrayCoverPhoto());
        newUserInfo.setPrivacy(PrivacyType.PUBLIC);

        userRepository.save(newUserInfo);

        return newUserInfo;
    }

    @Override
    public UserInfo update(AdditionalInfoDTO dto, String email) {
        UserInfo user = userRepository.getByLoginUsername(email);

        additionalInfoMapper.AdditionalInfoDTOToUserInfo(dto, user);
        return userRepository.save(user);
    }

    @Override
    public void adminUpdate(RegistrationDTO registrationDTO, String email) {
        UserInfo user = userRepository.getByLoginUsername(email);

        userMapper.registrationModelToUserInfoUpdate(registrationDTO, user);
        user.getLogin().setPassword(passwordEncoder.encode(registrationDTO.getPassword()));

        userRepository.save(user);
    }

    @Override
    public void delete(UserInfo userInfo) {
        userRepository.delete(userInfo);
    }

    @Override
    public UserInfo getUserById(int id) {
        return userRepository.getById(id);
    }

    @Override
    public UserInfo getByEmail(String email) {
        return userRepository.getByLoginUsername(email);
    }

    @Override
    public List<UserInfo> getAll() {
        return userRepository.findAll();
    }

    public boolean checkIfUserExists(Integer id) {
        return userRepository.existsById(id);
    }

    @Override
    public void changePassword(PasswordChangeDTO passwordChangeDTO) {
        UserInfo userInfo = userRepository.getById(passwordChangeDTO.getUserId());
        Login login = userInfo.getLogin();
        login.setPassword(passwordEncoder.encode(passwordChangeDTO.getNewPassword()));

        loginService.updatePassword(login);
    }

    @Override
    public List<UserInfo> getAllUsersByFirstNameOrLastName(String firstName, String lastName) {
        return userRepository.findByFirstNameStartingWithOrLastNameStartingWith(firstName, lastName);
    }

    private byte[] toByteArrayCoverPhoto() {
        //Add absolute path to generate first basic cover for new registered users
        //Viktor path:
        File file = new File("C:\\Users\\user\\Desktop\\social network\\social-network-team-12\\Social Network\\src\\main\\resources\\static\\images\\coverPhoto.png");
        //Antonio path:
//                File file = new File("C:\\Work\\GIT\\Final Project\\social-network-team-12\\Social Network\\src\\main\\resources\\static\\images\\blank.png");
        byte[] arr = new byte[2048];
        try {
            arr = Files.readAllBytes(file.toPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return arr;
    }

    private byte[] toByteArrayProfilePhoto() {
        //Add absolute path to generate first basic profile picture for new registered users
        //Viktor path:
        File file = new File("C:\\Users\\user\\Desktop\\social network\\social-network-team-12\\Social Network\\src\\main\\resources\\static\\images\\blank.png");
        //Antonio path:
//                File file = new File("C:\\Work\\GIT\\Final Project\\social-network-team-12\\Social Network\\src\\main\\resources\\static\\images\\blank.png");
        byte[] arr = new byte[2048];
        try {
            arr = Files.readAllBytes(file.toPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return arr;
    }
}
