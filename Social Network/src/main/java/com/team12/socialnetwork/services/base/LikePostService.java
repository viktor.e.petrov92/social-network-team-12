package com.team12.socialnetwork.services.base;

import com.team12.socialnetwork.entities.DTOmodels.LikePostDTO;
import com.team12.socialnetwork.entities.LikePost;

import java.security.Principal;

public interface LikePostService {

    void createLikePost(LikePost likePost);

    void deleteLikePost(LikePost likePost);

    int likePost(LikePostDTO postDTO, Principal principal);

    int dislikePost(LikePostDTO postDTO, Principal principal);
}
