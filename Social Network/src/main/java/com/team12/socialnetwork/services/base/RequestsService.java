package com.team12.socialnetwork.services.base;

import com.team12.socialnetwork.entities.Request;

import java.security.Principal;
import java.util.List;

public interface RequestsService {

    Request getBySenderIdAndReceiverId(int senderId, int receiverId);

    List<Request> getAllBySenderId(int senderId);

    List<Request> getAllByReceiverId(int receiverId);

    void sendFriendRequest(Principal principal, int receiverId);

    void acceptFriendRequest(Principal principal, int senderId);

    void declineFriendRequest(Principal principal, int senderId);
}
