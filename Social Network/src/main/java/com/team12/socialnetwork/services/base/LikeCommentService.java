package com.team12.socialnetwork.services.base;

import com.team12.socialnetwork.entities.DTOmodels.LikeCommentDTO;
import com.team12.socialnetwork.entities.LikeComment;

import java.security.Principal;

public interface LikeCommentService {

    void createLikeComment(LikeComment likeComment);

    void deleteLikeComment(LikeComment likeComment);

    int likeComment(LikeCommentDTO likeCommentDTO, Principal principal);

    int dislikeComment(LikeCommentDTO likeCommentDTO, Principal principal);
}
