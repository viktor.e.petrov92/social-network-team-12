package com.team12.socialnetwork.services;

import com.team12.socialnetwork.entities.Friend;
import com.team12.socialnetwork.entities.UserInfo;
import com.team12.socialnetwork.exceptions.DuplicateEntityException;
import com.team12.socialnetwork.repositories.base.FriendsRepository;
import com.team12.socialnetwork.services.base.FriendsService;
import com.team12.socialnetwork.services.base.UsersService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.List;

@Service
@RequiredArgsConstructor
public class FriendsServiceImpl implements FriendsService {
    private final FriendsRepository friendsRepository;
    private final UsersService usersService;

    private static final String ALREADY_FRIENDS = "You are already friends with this user";

    @Override
    public void createFriendship(Friend friend) {
        if (friendsRepository.existsById(friend.getFriendId())) {
            throw new DuplicateEntityException(ALREADY_FRIENDS);
        }

        friendsRepository.save(friend);
    }

    @Override
    public void deleteFriendship(Friend friend) {
        friendsRepository.delete(friend);
    }

    @Override
    public Friend getBySenderIdAndReceiverId(int userId, int friendId) {
        return friendsRepository.getBySenderIdAndReceiverId(userId, friendId);
    }

    @Override
    public List<Friend> getAllBySenderId(int senderId) {
        return friendsRepository.getAllBySenderId(senderId);
    }

    @Override
    public List<Friend> getAllByReceiverId(int receiverId) {
        return friendsRepository.getAllByReceiverId(receiverId);
    }

    @Override
    public void deleteFriend(Principal principal, int friendId) {
        UserInfo sender = usersService.getByEmail(principal.getName());
        UserInfo receiver = usersService.getUserById(friendId);

        Friend friendOne = friendsRepository.getBySenderIdAndReceiverId(sender.getId(), receiver.getId());
        Friend friendTwo = friendsRepository.getBySenderIdAndReceiverId(receiver.getId(), sender.getId());

        friendsRepository.delete(friendOne);
        friendsRepository.delete(friendTwo);
    }
}
