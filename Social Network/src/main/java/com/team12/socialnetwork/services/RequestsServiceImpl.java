package com.team12.socialnetwork.services;

import com.team12.socialnetwork.entities.Friend;
import com.team12.socialnetwork.entities.Request;
import com.team12.socialnetwork.entities.UserInfo;
import com.team12.socialnetwork.exceptions.DuplicateEntityException;
import com.team12.socialnetwork.repositories.base.RequestsRepository;
import com.team12.socialnetwork.services.base.FriendsService;
import com.team12.socialnetwork.services.base.RequestsService;
import com.team12.socialnetwork.services.base.UsersService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.List;

import static java.lang.String.format;

@Service
@RequiredArgsConstructor
public class RequestsServiceImpl implements RequestsService {

    private final RequestsRepository requestsRepository;
    private final UsersService usersService;
    private final FriendsService friendsService;

    private static final String INVITE_ALREADY_SENT = "Invite has already been sent to %s %s!";
    private static final String ALREADY_FRIENDS = "You are already friends with %s %s!";

    @Override
    public Request getBySenderIdAndReceiverId(int senderId, int receiverId) {
        return requestsRepository.getBySenderIdAndReceiverId(senderId, receiverId);
    }

    @Override
    public List<Request> getAllBySenderId(int senderId) {
        return requestsRepository.getAllBySenderId(senderId);
    }

    @Override
    public List<Request> getAllByReceiverId(int receiverId) {
        return requestsRepository.getAllByReceiverId(receiverId);
    }

    @Override
    public void sendFriendRequest(Principal principal, int receiverId) {
        UserInfo sender = usersService.getByEmail(principal.getName());
        UserInfo receiver = usersService.getUserById(receiverId);

        if (requestsRepository.getBySenderIdAndReceiverId(sender.getId(), receiver.getId()) != null) {
            throw new DuplicateEntityException(format(INVITE_ALREADY_SENT, receiver.getFirstName(), receiver.getLastName()));
        }
        if (friendsService.getBySenderIdAndReceiverId(sender.getId(), receiver.getId()) != null) {
            throw new DuplicateEntityException(format(ALREADY_FRIENDS, receiver.getFirstName(), receiver.getLastName()));
        }

        Request request = new Request(sender, receiver);
        requestsRepository.save(request);
    }

    @Override
    public void acceptFriendRequest(Principal principal, int senderId) {
        UserInfo receiver = usersService.getByEmail(principal.getName());
        UserInfo sender = usersService.getUserById(senderId);

        if (friendsService.getBySenderIdAndReceiverId(sender.getId(), receiver.getId()) != null) {
            throw new DuplicateEntityException(format(ALREADY_FRIENDS, receiver.getFirstName(), receiver.getLastName()));
        }

        Friend friendOne = new Friend(sender, receiver);
        Friend friendTwo = new Friend(receiver, sender);
        friendsService.createFriendship(friendOne);
        friendsService.createFriendship(friendTwo);

        Request request = requestsRepository.getBySenderIdAndReceiverId(sender.getId(), receiver.getId());
        requestsRepository.delete(request);
    }

    @Override
    public void declineFriendRequest(Principal principal, int senderId) {
        UserInfo receiver = usersService.getByEmail(principal.getName());
        UserInfo sender = usersService.getUserById(senderId);

        Request request = requestsRepository.getBySenderIdAndReceiverId(sender.getId(), receiver.getId());
        requestsRepository.delete(request);
    }
}
