package com.team12.socialnetwork.services.base;

import com.team12.socialnetwork.entities.DTOmodels.RegistrationDTO;
import com.team12.socialnetwork.entities.Login;

public interface LoginService {

    Login findByUsername(String username);

    void createLogin(RegistrationDTO registrationDTO);

    boolean existsByUsername(String email);

    void updatePassword(Login login);
}
