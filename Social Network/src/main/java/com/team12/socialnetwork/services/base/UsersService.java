package com.team12.socialnetwork.services.base;

import com.team12.socialnetwork.entities.DTOmodels.AdditionalInfoDTO;
import com.team12.socialnetwork.entities.DTOmodels.PasswordChangeDTO;
import com.team12.socialnetwork.entities.DTOmodels.RegistrationDTO;
import com.team12.socialnetwork.entities.UserInfo;

import java.util.List;

public interface UsersService {

    UserInfo create(RegistrationDTO registrationDTO);

    UserInfo update(AdditionalInfoDTO dto, String email);

    void adminUpdate(RegistrationDTO registrationDTO, String email);

    void delete(UserInfo userInfo);

    UserInfo getUserById(int id);

    UserInfo getByEmail(String email);

    List<UserInfo> getAll();

    boolean checkIfUserExists(Integer id);

    void changePassword(PasswordChangeDTO passwordChangeDTO);

    List<UserInfo> getAllUsersByFirstNameOrLastName(String firstName, String lastName);
}
