"use strict";
$(function () {
    changePassword();
});

function changePassword() {
    $(".editing-info").on("click", "#update", function () {
        let $userId = document.getElementById("userId").valueOf().value;
        let $currentPassword = $("#currentPassword").val();
        let $newPassword = $("#newPassword").val();
        let $confirmPassword = $("#confirmPassword").val();


        let requestBody = {
            "userId": $userId,
            "currentPassword": $currentPassword,
            "newPassword": $newPassword,
            "confirmPassword": $confirmPassword,
        };

        fetch("/api/users/edit-pass", {
            method: "POST",
            body: JSON.stringify(requestBody),
            headers: {
                "Content-type": "application/json",
                "Accept": "application/json, text/plain, */*"
            }
        })
            .then((response) => {
                if (response.status !== 200) {
                    toastr.error('Invalid input! Please make sure are fields are correct and match the requirements.');
                } else {
                    toastr.info('You have successfully changed your password!');
                }
            });
    });
}