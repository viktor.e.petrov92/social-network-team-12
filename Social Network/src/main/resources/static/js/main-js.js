"use strict";
$(function () {
    likePost();
    postComment();
    sendRequest();
    showMore();
    acceptRequest();
    declineRequest();
    deleteFriend();
    createPost();
    likeComment();
    searchUserByFirstName();
});

function showMore() {
    $(".loadMore").on("click", ".showmore", function () {
        let postIdNumber = $(this).closest(".postIdForLike").attr("id").substr(5);

        let expand = document.getElementById("expand-" + postIdNumber);

        let hideShowDiv = document.getElementById("show-" + postIdNumber);

        let divNumber = hideShowDiv.getAttribute("id").substr(5);

        if (hideShowDiv.style.display === 'none' && postIdNumber === divNumber) {
            expand.innerHTML = expand.innerHTML.replace("Show more comments", "Show less comments");
            hideShowDiv.style.display = 'block';
        } else {
            expand.innerHTML = expand.innerHTML.replace("Show less comments", "Show more comments");
            hideShowDiv.style.display = 'none';
        }
    })
}

function postComment() {
    $(".loadMore").on("click", ".submitComment", function () {

        let totalComments = $(this).closest(".postIdForLike").find(".total-comments");
        let totalCommentsVal = parseInt(totalComments.text());

        let postId = $(this).closest(".postIdForLike").attr("id").substr(5);
        let postSelector = $(this).closest(".postIdForLike").attr("id");
        let content = $(this).parent().find(".comment-content").val();
        let requestBody = {
            "content": content,
            "postId": postId
        };

        console.log(totalCommentsVal);

        fetch(
            "/api/comments/", {
                method: "POST",
                body: JSON.stringify(requestBody),
                headers: {
                    "Content-type": "application/json",
                    "Accept": "application/json, text/plain, */*"
                }
            })
            .then((res) => res.json())
            .then(function (data) {

                $(".comment-content").val('');

                $("#posts").find("#" + postSelector + " .we-comet").prepend(
                    "<li>" +
                    "<div class='comet-avatar'>" +
                    "<img src=' " + "data:image/png;base64," + data.creator.profilePicture + " '  alt=''>" +
                    "</div>" +
                    "<div class='commentId commentIdForLike' id='com-id-" + data.id + "'>" +
                    "<div class='coment-head'>" +
                    "<h5>" + data.creator.firstName + ' ' + data.creator.lastName + " </h5>" +
                    "<span>" + moment().fromNow() + " </span>" +
                    "<span class='comment' id='comment' data-toggle='tooltip'  title='Comments'>" +
                    "    <i class='ti-heart  comment-like  notLiked '></i>" +
                    "     <ins class='comments-likes' id='comments-likes'>" + data.totalLikes + "</ins>" +
                    "   </span>" +
                    "</div>" +
                    "<p>" + data.content + "</p>" +
                    "</div>" +
                    "</li>");

                totalComments.text(totalCommentsVal + 1);
            }).catch(function () {
            alert("Cannot create an empty comment!");
        });
    });
}

function likePost() {
    $(".loadMore").on("click", ".post-like", function () {
            let postIdNumber = $(this).closest(".postIdForLike").attr("id").substr(5);

            let $totalLikes = $(this).closest(".like").find(".totalLikes");

            let requestBody = {
                "postId": postIdNumber
            };

            if ($(this).hasClass('notLiked')) {
                $(this).removeClass('notLiked');
                $(this).addClass("isLiked");
                fetch("/api/postlikes/like/", {
                    method: "POST",
                    body: JSON.stringify(requestBody),
                    headers: {
                        "Content-type": "application/json",
                        "Accept": "application/json, text/plain, */*"
                    }
                }).then((res) => {
                    if (res.status !== 200) {
                        alert("You have already liked that post! Click one more time for dislike!")
                    } else {
                        res.json()
                            .then(function (el) {
                                $totalLikes.text(el);
                            });
                    }
                });
            } else {
                $(this).removeClass("isLiked");
                $(this).addClass("notLiked");
                fetch("/api/postlikes/dislike/", {
                    method: "POST",
                    body: JSON.stringify(requestBody),
                    headers: {
                        "Content-type": "application/json",
                        "Accept": "application/json, text/plain, */*"
                    }
                }).then((res) => res.json())
                    .then(function (el) {
                        $totalLikes.text(el);
                    });
            }
        }
    );
}

function sendRequest() {
    $(".add-btn").on("click", function () {
        let receiverId = document.getElementById("receiver-id").valueOf().value;
        let text = document.getElementById("a-text");
        console.log(receiverId);
        $.ajax({
            url: "/api/requests",
            type: 'POST',
            data: {
                receiverId: receiverId
            },
            success: function (response) {
                console.log(response);
                text.innerHTML = "Request Sent"
            }
        });
    })
}

function acceptRequest() {
    $(".dropdowns").on("click", ".accept-req", function () {
        let senderId = $(this).closest(".sender-id").attr("id").substr(4);

        let hideRequest = document.getElementById("req-" + senderId);
        $.ajax({
            url: "/api/requests/accept",
            type: 'POST',
            data: {
                senderId: senderId
            },
            success: function (response) {
                console.log(response);
                hideRequest.style.display = 'none';
            }
        });
    })
}

function deleteFriend() {
    $(".del-btn").on("click", function () {
        let receiverId = document.getElementById("receiver-id2").valueOf().value;
        console.log(receiverId);
        let text = document.getElementById("aa-text");
        console.log(receiverId);
        $.ajax({
            url: "/api/friends",
            type: 'POST',
            data: {
                receiverId: receiverId
            },
            success: function (response) {
                console.log(response);
                text.innerHTML = "Friend Deleted"
            }
        });
    })
}

function declineRequest() {
    $(".dropdowns").on("click", ".decline-req", function () {
        let senderId = $(this).closest(".sender-id").attr("id").substr(4);
        console.log(senderId);
        let hideRequest = document.getElementById("req-" + senderId);
        console.log(hideRequest);
        $.ajax({
            url: "/api/requests/decline",
            type: 'POST',
            data: {
                senderId: senderId
            },
            success: function (response) {
                console.log(response);
                hideRequest.style.display = 'none';
            }
        });
    })
}

function createPost() {
    $(".new-postbox").on("click", ".new-post", function () {
        let content = $(this).closest(".newpst-input").find(".post-content").val();

        // let file = $("#post-file")[0].files[0];

        function getBase64(file) {
            return new Promise((resolve, reject) => {
                const reader = new FileReader();
                reader.readAsDataURL(file);
                reader.onload = () => resolve(reader.result);
                reader.onerror = error => reject(error);
            });
        }

        var file = document.querySelector('input[name="post-file"]').files[0];
        let privacyType = $(this).closest(".newpst-input").find(".privacy-type").val();
        if (document.querySelector('input[name="post-file"]').files.length === 0) {
            let requestBody = {
                "content": content,
                "locationPost": "string",
                "pictureValue": null,
                "privacy": privacyType,
                "videoPost": "string"
            };

            fetch(
                "/api/posts/", {
                    method: "POST",
                    body: JSON.stringify(requestBody),
                    headers: {
                        "Content-type": "application/json",
                        "Accept": "application/json, text/plain, */*"
                    }
                })
                .then((res) => res.json())
                .then(function (post) {
                    let $posts = $("#posts");
                    $posts.prepend(
                        " <div id='post-" + post.id + "' class=\"central-meta item postIdForLike\">" +
                        "<div class='user-post'>" +
                        " <div class='friend-info'>" +
                        " <div class='friend-name'>" +
                        "   <ins><a href='/profile' title=''>" + post.createdBy.firstName + ' ' + post.createdBy.lastName + "</a></ins>" +
                        "    <span>" + post.timeCreated + "</span>" +
                        " </div>" +
                        "<div class='post-meta'>" +
                        "<img src='" + "data:image/png;base64," + post.picturePost + " ' alt=''>" +
                        "<div class='description'>" +
                        "<p  id='commentcont' >" + post.content + "</p>" +
                        "  </div>" +
                        "<div class='we-video-info '>" +
                        " <ul>" +
                        "  <li>" +
                        "   <span class='comment' id='comment' data-toggle='tooltip'  title='Comments'>" +
                        "    <i class='fa fa-comments-o'></i>" +
                        "     <ins class='total-comments' id='total-comments'>" + 0 + "</ins>" +
                        "   </span>" +
                        "  </li>" +
                        "  <li>" +
                        "   <span class='like' data-toggle='tooltip' title='like'>" +
                        "    <i class='ti-heart post-like notLiked' id='" + post.id + "'></i>" +
                        "      <ins class='totalLikes '>" + 0 + "</ins>" +
                        "   </span>" +
                        "  </li>" +
                        " </ul>" +
                        "</div>" +
                        "</div>" +
                        "  <div class='coment-area' id='comments'>" +
                        "  <ul class='we-comet'>" +
                        "</ul>" +
                        "   <li>" +
                        "  <a id='expand-" + post.id + "' class='showmore underline'> Show more comments </a>" +
                        "<div id='show-" + post.id + "' class='comment-div showHide' style='display:none'> " +
                        "</div>" +
                        " </li>" +
                        "<li class='post-comment'>" +
                        "<div class='post-comt-box'>" +

                        "<textarea placeholder='Post your comment' class='comment-content' id='comment-content'></textarea>" +
                        "<button type='submit' id='submitComment' class='submitComment'>Comment</button>" +
                        "</div>" +
                        "</li>" +
                        "</div>" +
                        "</div>" +
                        "</div>"
                    );
                }).catch(function () {
                alert("Cannot create an empty post!");
            });
        } else {
            getBase64(file).then(
                data => {
                    console.log(data);
                    let requestBody = {
                        "content": content,
                        "locationPost": "string",
                        "pictureValue": data,
                        "privacy": privacyType,
                        "videoPost": "string"
                    };

                    fetch(
                        "/api/posts/", {
                            method: "POST",
                            body: JSON.stringify(requestBody),
                            headers: {
                                "Content-type": "application/json",
                                "Accept": "application/json, text/plain, */*"
                            }
                        })
                        .then((res) => res.json())
                        .then(function (post) {
                            let $posts = $("#posts");
                            $posts.prepend(
                                " <div id='post-" + post.id + "' class=\"central-meta item postIdForLike\">" +
                                "<div class='user-post'>" +
                                " <div class='friend-info'>" +
                                "  <figure>" +
                                "    <img id='image' src=' " + "data:image/png;base64," + post.createdBy.profilePicture + " ' alt=''>" +
                                "  </figure>" +
                                " <div class='friend-name'>" +
                                "   <ins><a href='/profile' title=''>" + post.createdBy.firstName + ' ' + post.createdBy.lastName + "</a></ins>" +
                                "    <span>" + post.timeCreated + "</span>" +
                                " </div>" +
                                "<div class='post-meta'>" +
                                "<img src='" + "data:image/png;base64," + post.picturePost + " ' alt=''>" +
                                "<div class='description'>" +
                                "<p  id='commentcont' >" + post.content + "</p>" +
                                "  </div>" +
                                "<div class='we-video-info '>" +
                                " <ul>" +
                                "  <li>" +
                                "   <span class='comment' id='comment' data-toggle='tooltip'  title='Comments'>" +
                                "    <i class='fa fa-comments-o'></i>" +
                                "     <ins class='total-comments' id='total-comments'>" + 0 + "</ins>" +
                                "   </span>" +
                                "  </li>" +
                                "  <li>" +
                                "   <span class='like' data-toggle='tooltip' title='like'>" +
                                "    <i class='ti-heart post-like notLiked' id='" + post.id + "'></i>" +
                                "      <ins class='totalLikes '>" + 0 + "</ins>" +
                                "   </span>" +
                                "  </li>" +
                                " </ul>" +
                                "</div>" +
                                "</div>" +
                                "  <div class='coment-area' id='comments'>" +
                                "  <ul class='we-comet'>" +
                                "</ul>" +
                                "   <li>" +
                                "  <a id='expand-" + post.id + "' class='showmore underline'> Show more comments </a>" +
                                "<div id='show-" + post.id + "' class='comment-div showHide' style='display:none'> " +
                                "</div>" +
                                " </li>" +
                                "<li class='post-comment'>" +
                                "<div class='comet-avatar'> " +
                                "<img src=' " + "data:image/png;base64," + post.createdBy.profilePicture + " ' alt=''>" +
                                "</div>" +
                                "<div class='post-comt-box'>" +

                                "<textarea placeholder='Post your comment' class='comment-content' id='comment-content'></textarea>" +
                                "<button type='submit' id='submitComment' class='submitComment'>Comment</button>" +
                                "</div>" +
                                "</li>" +
                                "</div>" +
                                "</div>" +
                                "</div>"
                            );
                        }).catch(function () {
                        alert("Cannot create an empty post!");
                    })
                })
        }
    });
}

function likeComment() {
    $(".loadMore").on("click", ".comment-like", function () {
            let commentId = $(this).closest(".commentIdForLike").attr("id").substr(7);

            let $totalLikes = $(this).closest(".coment-head").find(".comments-likes");

            let requestBody = {
                "commentId": commentId
            };

            if ($(this).hasClass('notLiked')) {
                $(this).removeClass('notLiked');
                $(this).addClass("isLiked");
                fetch("/api/comment/like/", {
                    method: "POST",
                    body: JSON.stringify(requestBody),
                    headers: {
                        "Content-type": "application/json",
                        "Accept": "application/json, text/plain, */*"
                    }
                }).then((res) => {
                    if (res.status !== 200) {
                        alert("You have already liked that comment! Click one more time for dislike!")
                    } else {
                        res.json()
                            .then(function (el) {
                                $totalLikes.text(el);
                            });
                    }
                });
            } else {
                $(this).removeClass("isLiked");
                $(this).addClass("notLiked");
                fetch("/api/comment/dislike/", {
                    method: "POST",
                    body: JSON.stringify(requestBody),
                    headers: {
                        "Content-type": "application/json",
                        "Accept": "application/json, text/plain, */*"
                    }
                }).then((res) => res.json())
                    .then(function (el) {
                        $totalLikes.text(el);
                    });
            }
        }
    );
}

function searchUserByFirstName() {
    $("#search-input").keyup(function () {
        let searchBoxValue = this.value;
        if (searchBoxValue.length === 0) {
            getAndLoadUsers();
        } else {
            const url = new URL("http://localhost:8080/api/users/filter");
            let params = {firstName: searchBoxValue, lastName: searchBoxValue};
            url.search = new URLSearchParams(params).toString();

            fetch(url, {
                method: "GET",
                headers: {
                    "Content-type": "application/json",
                    "Accept": "application/json, text/plain, */*"
                }
            })
                .then((res) => res.json())
                .then(function (el) {
                    if (el.length === 0) {
                        $("#results-list").empty();
                    } else {
                        let $tableContent = $("#results-list");
                        $tableContent.empty();
                        el.forEach(function (el) {
                            $tableContent.append(
                                "  <li>" +
                                "    <a title='' href='/profile/" + el.id + "' onclick=\"window.open('http://localhost:8080/profile/" + el.id + "')\" >" +
                                " <img   class='search-image' href= '/profile/" + el.id + "'  src='" + "data:image/png;base64," + el.profilePicture + "' alt=''>" +
                                "    <div class='mesg-meta'  >" +
                                "    <h5>" + el.firstName + ' ' + el.lastName + "</h5>" +
                                "   </div>" +
                                "     </a>" +
                                "            </li>"
                            );
                        });
                    }
                })
                .catch((err) => {
                    console.log(err.message);
                });
        }
    })
}

function getAndLoadUsers() {
    $.ajax({
        url: window.location.origin + "/api/users/",
        type: "GET",

        success: function (response) {
            console.log(response);
            let $tableContent = $("#results-list");
            $tableContent.empty();
            response.forEach(function (el) {
                console.log(el);
                $tableContent.append(
                    "  <li>" +
                    "    <a title='' href='/profile/" + el.id + "'  onclick=\"window.open('http://localhost:8080/profile/" + el.id + "')\">" +
                    " <img  class='search-image' src='" + "data:image/png;base64," + el.profilePicture + "' alt=''  >" +
                    "    <div class='mesg-meta'>" +
                    "    <h5>" + el.firstName + ' ' + el.lastName + "</h5>" +
                    "   </div>" +
                    "     </a>" +
                    "            </li>"
                );
            });
        },
        error: function () {
            console.log("error");
        }
    })
}