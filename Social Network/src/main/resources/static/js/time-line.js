"use strict";
$(function () {
    getAndLoadContent();
    getAndLoadRequests();
});

function getAndLoadContent() {
    $.ajax({
        url: "/api/posts/time-line/",
        type: "GET",
        success: function (response) {
            let $posts = $("#posts");
            response.forEach(function (post) {
                $posts.prepend(
                    " <div id='post-" + post.id + "' class=\"central-meta item postIdForLike\">" +
                    "<div class='user-post'>" +
                    " <div class='friend-info'>" +
                    "  <figure>" +
                    "    <img id='image' src=' " + "data:image/png;base64," + post.createdBy.profilePicture + " ' alt=''>" +
                    "  </figure>" +
                    " <div class='friend-name'>" +
                    "   <ins><a href='/profile/" + post.createdBy.id + "' title=''>" + post.createdBy.firstName + ' ' + post.createdBy.lastName + "</a></ins>" +
                    "    <span>" + post.timeCreated + "</span>" +
                    " </div>" +
                    "<div class='post-meta'>" +
                    "<img src='" + "data:image/png;base64," + post.picturePost + " ' alt=''>" +
                    "<div class='description'>" +
                    "<p  id='commentcont' >" + post.content + "</p>" +
                    "  </div>" +
                    "<div class='we-video-info'>" +
                    " <ul>" +
                    "  <li>" +
                    "   <span class='comment' id='comment' data-toggle='tooltip'  title='Comments'>" +
                    "    <i class='fa fa-comments-o '></i>" +
                    "     <ins class='total-comments' id='total-comments'>" + post.comments.length + "</ins>" +
                    "   </span>" +
                    "  </li>" +
                    "  <li>" +
                    "   <span class='like' data-toggle='tooltip' title='like'>" +
                    "    <i class='ti-heart post-like notLiked' id='" + post.id + "'></i>" +
                    "      <ins class='totalLikes '>" + post.totalLikes + "</ins>" +
                    "   </span>" +
                    "  </li>" +
                    " </ul>" +
                    "</div>" +
                    "</div>" +
                    "  <div class='coment-area' id='comments'>" +
                    "  <ul class='we-comet'>" +
                    "</ul>" +
                    "   <li>" +
                    "  <a id='expand-" + post.id + "' class='showmore underline'> Show more comments </a>" +
                    "<div id='show-" + post.id + "' class='comment-div showHide' style='display:none'> " +
                    "</div>" +
                    " </li>" +
                    "<li class='post-comment'>" +
                    "<div class='post-comt-box'>" +

                    "<textarea placeholder='Post your comment' class='comment-content' id='comment-content'></textarea>" +
                    "<button type='submit' id='submitComment' class='submitComment'>Comment</button>" +
                    "</div>" +
                    "</li>" +
                    "</div>" +
                    "</div>" +
                    "</div>"
                );

                let postIdApp = "post-" + post.id;
                let array = post.comments;
                let i;
                for (i = 0; i < post.comments.length; i++) {
                    if (i < post.comments.length - 2) {
                        $("#posts").find("#" + postIdApp + " .showHide").prepend(
                            "<li>" +
                            "<div class='comet-avatar'>" +
                            "<img src=' " + "data:image/png;base64," + array[i].creator.profilePicture + " '  alt=''>" +
                            "</div>" +
                            "<div class='commentId commentIdForLike' id='com-id-" + array[i].id + "'>" +
                            "<div class='coment-head'>" +
                            "<h5>" + array[i].creator.firstName + ' ' + array[i].creator.lastName + " </h5>" +
                            "<span>" + array[i].timeCreated + "</span>" +
                            "<span class='comment' id='comment' data-toggle='tooltip'  title='Comments'>" +
                            "    <i class='ti-heart  comment-like  notLiked '></i>" +
                            "     <ins class='comments-likes' id='comments-likes'>" + array[i].totalLikes + "</ins>" +
                            "   </span>" +
                            "</div>" +
                            "<p>" + array[i].content + "</p>" +
                            "</div>" +
                            "</li>")
                    } else {
                        $("#posts").find("#" + postIdApp + " .we-comet").prepend(
                            "<li>" +
                            "<div class='comet-avatar'>" +
                            "<img src=' " + "data:image/png;base64," + array[i].creator.profilePicture + " '  alt=''>" +
                            "</div>" +
                            "<div class='commentId commentIdForLike' id='com-id-" + array[i].id + "'>" +
                            "<div class='coment-head'>" +
                            "<h5>" + array[i].creator.firstName + ' ' + array[i].creator.lastName + " </h5>" +
                            "<span>" + array[i].timeCreated + "</span>" +
                            "<span class='comment' id='comment' data-toggle='tooltip'  title='Comments'>" +
                            "    <i class='ti-heart  comment-like  notLiked '></i>" +
                            "     <ins class='comments-likes' id='comments-likes'>" + array[i].totalLikes + "</ins>" +
                            "   </span>" +
                            "</div>" +
                            "<p>" + array[i].content + "</p>" +
                            "</div>" +
                            "</li>"
                        );
                    }
                }
            });
        },
        error: function () {
            toastr.error('Something went wrong.');
        }
    })
}

function getAndLoadRequests() {
    let userId = document.getElementById("logged-user").valueOf().value;
    $.ajax({
        url: "/api/requests/receiver/" + userId,
        type: "GET",
        success: function (response) {
            let $requests = $("#requests");
            response.forEach(function (request) {
                $requests.append(
                    "<li href= '/profile/" + request.sender.id + "' style='display: block' id='req-" + request.sender.id + "'>" +
                    "<a id='blockreq'>" +
                    "<div  id='req-" + request.sender.id + "' class='sender-id' >" +
                    "<img id='req-pic' alt='' href= '/profile/" + request.sender.id + "' onclick=\"window.open('http://localhost:8080/profile/" + request.sender.id + "')\" src=' " + "data:image/png;base64," + request.sender.profilePicture + " '>" +
                    "<h6 id='req-names'>" + request.sender.firstName + ' ' + request.sender.lastName + "</h6>" +
                    "<button class='accept-req rqbuttonss' >Accept</button>" +
                    "<button class='decline-req rqbuttonss' >Decline</button>" +
                    "</div>" +
                    "</a>" +
                    "</li>"
                );
            })
        }
    })
}

